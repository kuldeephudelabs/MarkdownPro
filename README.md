![Image of Yaktocat](https://odesk-prod-portraits.s3.amazonaws.com/Companies:848990:CompanyLogoURL?AWSAccessKeyId=1XVAX3FNQZAFC9GJCFR2&Expires=2147483647&Signature=wt8a0IjJb5pn2F8%2Be99YXqe6QKY%3D)
# Quize Api
Quize api enable users to fetch questions categorized into categories and topics
## Getting Started
These instruction will get you a copy of the project up and running on your local machine for development and testing purpose. See deployment for notes on how to deploy a project on a live system.
### Prerequisites

What things you need to install the software and how to install them. you will need   [Node.js >=7.0.0  ]() and [NPM >=4.0.0] ()  Run the following commands to setup a local copy of the software.
 
```
   git clone https://github.com/pareshchouhan/test-git.git
```
### Installing 
To get your server up and running run the following commands. make sure you have cloned the [repository ]()  before proceeding.
```
 cd test  -git
npm install
npm start
```
wait until you see the following output
```
INFO   - Building documentation.....
INFO - Cloning site directory
[I 170309 16:07:05 server:283] Serving on http://127.0.0.1:8000
[1 170309 16:07:05 handlers:60] Start watching changes
[I 170309 16:07:05 handlers:62] Start detecting changes
```
Now Open a web browser and run [http://hudelabs.local/phpmyadmin](http://hudelabs.local/phpmyadmin) to see if everything is up and running.
## Running the tests
To run tests run the following commands
```
 npm install 
 npm run test 
```

```javascript
var i=0;
for (i=30;i>0;i++){
console.log('it works!');

}
```
## Deployment 
To deploy the system on production servers.
* Make sure you have  [ Node.js ]()  Installed
* Requires Kernel Version 4.4.0
* REquires Php-fpm installed
* REquired php 7.1

## Built With 
*   [ Dropwizard ]()  - The web framework used
*  [Maven ]() - Dependency Managment
*   [ROME ]()  - Used to generate RSS Feeds.

## Versioning
We Use  [ SemVer]()  for versioning. for the versioning available.see the  [ tags on this repository ].

## Authors 
*  **Your Name Here** -Initial work - [ YourNameHere ]()

See also the list of   [contributors ]()  who participated in this project.

## License 
This Project is licensed under the MIT License - see the  [ LICENSE.md  ]()  file for details

## Acknowledgments
* Hattip to anyone who's code was used
* Inspiration
* etc

